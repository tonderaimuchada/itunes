//
//  ViewController.swift
//  iTunes
//
//  Created by Tonderai Muchada on 17/9/2019.
//  Copyright © 2019 Tonderai Muchada. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {

//    let spinner = CustomSpinner()
//    spinner.showSpinner()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func next(_ sender: Any) {
        performSearchRequest()
        performSegue(withIdentifier: "goToResults", sender: self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    let requestURL = "https://itunes.apple.com/search"
    let requestVariablesDictionary = [
    "term":"spiderman",
    "entity":"movie",
    "limit":"4"
    ]
    
    func performSearchRequest(){
        Alamofire.request(requestURL, method: .get, parameters: requestVariablesDictionary, encoding: URLEncoding(destination: .queryString), headers: nil)
            .responseJSON { response in
                do{
                    print(response)
                    let json = try JSON(data: response.data!)
                    let transactionSuccess = json["Success"]
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                        //code to be delayed
                        self.view.backgroundColor = .clear
//                        spinner.hideSpinner()
//                        self.performSegue(withIdentifier: "goToTransactionStatus", sender: self)
                    }
                }catch{
                    print("Error in making the request")
                }
        }
    }

}
